function Queue() {
	this.first = null;
	this.last = null;
	this.length = 0;

	this.enqueue = function(value) {
		let n = new Node(value);
		if(this.first == null) {
			this.first = n;
			this.last = n;
		}else {
			this.last.next = n;
			this.last = this.last.next;
		}
		this.length++;
	};

	this.dequeue = function() {
		if(this.first == null) {
			return null;
		}
		let temp = this.first;
		this.first = this.first.next;
		this.length--;
		return temp.value;
	};

	this.peek = function() {
		if(this.first == null) {
			return null;
		}
		return this.first.value;
	};

	this.items = function() {
		let arr = [];
		let temp = this.first;
		while(temp != null) {
			arr.push(temp.value);
			temp = temp.next;
		}
		return arr;
	};
}