function Stack() {
	this.top = null;
	this.length = 0;

	this.push = function(value) {
		let n = new Node(value);
		if(this.top == null) {
			this.top = n;
		}else {
			n.next = this.top;
			this.top = n;
		}
		this.length++;
	};

	this.pop = function() {
		if(this.top == null) {
			return null;
		}
		let temp = this.top;
		this.top = this.top.next;
		this.length--;
		return temp.value;
	};

	this.peek = function() {
		if(this.top == null) {
			return null;
		}
		return this.top.value;
	};

	this.items = function() {
		let arr = [];
		let temp = this.top;
		while(temp != null) {
			arr.unshift(temp.value);
			temp = temp.next;
		}
		return arr;
	};
}