function Node(value) {
	this.value = value;
	this.next = null;
}

function LinkedList() {
	this.head = null;
	this.tail = null;
	this.length = 0;

	this.add = function(value) {
		let n = new Node(value);
		if(this.head == null) {
			this.head = n;
			this.tail = n;
		}else {
			this.tail.next = n;
			this.tail = this.tail.next;
		}
		this.length++;
	};

	this.remove = function(value) {
		if(this.head.value == value) {
			this.head = this.head.next;
		}else {
			let temp = this.head;
			while(temp.next != null) {
				if(temp.next.value == value) {
					if(temp.next == this.tail) {
						tail = temp;
					}
					temp.next = temp.next.next;
				}
				temp = temp.next;
				if(temp == null) {
					break;
				}
			}
		}
		this.length--;
	};

	this.contains = function(value) {
		let temp = this.head;
		while(temp != null) {
			if(temp.value == value) {
				return true;
			}
			temp = temp.next;
		}
		return false;
	};

	this.clear = function() {
		this.head = null;
		this.tail = null;
		this.length = 0;
	};

	this.items = function() {
		let arr = [];
		let temp = this.head;
		while(temp != null) {
			arr.push(temp.value);
			temp = temp.next;
		}
		return arr;
	};
}